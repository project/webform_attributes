INTRODUCTION
------------

This module allows you to add html5 attributes to your form elements.

More information:

* https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Using_data_attributes
* http://html5doctor.com/html5-forms-introduction-and-new-attributes/
* https://www.sitepoint.com/use-html5-data-attributes/
* http://html5doctor.com/html5-custom-data-attributes/
* https://www.html5rocks.com/en/tutorials/forms/html5forms/#toc-other-form-attr
* http://updates.html5rocks.com/2015/06/checkout-faster-with-autofill

REQUIREMENTS
------------

This module requires the following modules:

 *Webform (https://www.drupal.org/project/webform)

INSTALLATION
------------

 *Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

  * Configure each attribute in the display section when editing a webform
  component

SUPPORT
-------

  Please use the issue queue for filing bugs with this module at
  http://drupal.org/project/issues/webform_attributes

MAINTAINERS
-----------

  Current maintainers:
  *Fabricio Nascimento (fadonascimento)- https://www.drupal.org/u/fadonascimento
  *Leonardo Jose Carnieri (ljcarnieri) - https://www.drupal.org/u/ljcarnieri
