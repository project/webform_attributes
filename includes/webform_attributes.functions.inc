<?php

/**
 * @file
 * Functions related the Webform Attributes.
 */

/**
 * Callback form submit.
 */
function webform_attributes_configure_form_submit(&$form, &$form_state) {
  $form_values = $form_state['values'];
  if (!isset($form_values['webform_attributes_form'])) {
    return FALSE;
  }

  $node = $form['#node'];
  $attributes = $form_values['webform_attributes_form'];
  if (empty($attributes)) {
    // Delete record.
    return _webform_attributes_delete_record($node->nid);
  }
  // Parser values.
  $attributes = _webform_attributes_parser_string_to_array($attributes);
  // Insert/update record.
  _webform_attributes_save_record($node->nid, $attributes);
}

/**
 * Find an existing record.
 *
 * @param int $nid
 *   Node nid.
 * @param bool $parser
 *   If true, parser the array to string.
 *
 * @return array|string
 *   Return array with attributes when found it.
 */
function _webform_attributes_find_record_by_nid($nid, $parser = TRUE) {
  $result = db_select('webform_attributes', 'wa')
    ->fields('wa', array('attributes'))
    ->condition('nid', $nid, '=')
    ->execute()
    ->fetchField();

  if (!empty($result)) {
    $result = unserialize($result);
    if ($parser) {
      return _webform_attributes_parser_array_to_string($result);
    }
  }
  return $result;
}

/**
 * Insert a new record or update an existing record.
 *
 * @param int $nid
 *   Node nid.
 * @param array $attributes
 *   Form attributes.
 *
 * @return MergeQuery
 *   A new MergeQuery object for this connection.
 */
function _webform_attributes_save_record($nid, array $attributes) {
  return db_merge('webform_attributes')
    ->key(array(
      'nid' => $nid,
    ))
    ->fields(array(
      'attributes' => serialize($attributes),
      'changed'    => REQUEST_TIME,
    ))
    ->execute();
}

/**
 * Remove an existing record.
 *
 * @param int $nid
 *   Node nid.
 *
 * @return DeleteQuery
 *   A new DeleteQuery object for this connection.
 */
function _webform_attributes_delete_record($nid) {
  return db_delete('webform_attributes')
    ->condition('nid', $nid)
    ->execute();
}

/**
 * Alter render for all components.
 *
 * @param array $element
 *   The Drupal form element.
 */
function _webform_attributes_webform_component_render_alter_all_components(array &$element) {

  $extra_fields = array(
    'webform_attributes_data_field' => array(
      'field' => 'webform_attributes_data_field',
      'element_key' => '#attributes',
    ),
    'webform_attributes_wrapper_data_field' => array(
      'field' => 'webform_attributes_wrapper_data_field',
      'element_key' => '#wrapper_attributes',
    ),
  );

  foreach ($extra_fields as $field) {
    _webform_attributes_apply_html_attributes_on_the_component($element, $field);
  }
}

/**
 * Apply html attributes on component.
 *
 * @param array $element
 *   The Drupal form element.
 * @param array $extra_field
 *   The name of extra field.
 */
function _webform_attributes_apply_html_attributes_on_the_component(array &$element, array $extra_field) {

  $field = $extra_field['field'];
  if (!empty($element['#webform_component']['extra'][$field])) {
    $attributes = $element['#webform_component']['extra'][$field];
    $attributes = _webform_attributes_parser_string_to_array($attributes);
    _webform_attributes_inside_attributes($element, $attributes, $extra_field['element_key']);
  }
}

/**
 * Form alter for all components.
 */
function _webform_attributes_form_webform_component_edit_form_alter_all_components(&$form, $node) {

  $attribute_for_input = array(
    '#type'          => 'textarea',
    '#default_value' => _webform_attributes_get_value_extra($node, 'webform_attributes_data_field'),
    '#title'         => t('Input HTML attributes'),
    '#description'   => t('Key-value pairs MUST be specified as "attribute_name|attribute_value". Use of only alphanumeric characters and underscores is recommended in keys. One attribute per line.'),
    '#required'      => FALSE,
    '#weight'        => -8,
    '#element_validate' => array('_webform_attributes_validate_options_attributes'),
    '#parents' => array('extra', 'webform_attributes_data_field'),
  );

  $attribute_for_wrapper = array(
    '#type'          => 'textarea',
    '#default_value' => _webform_attributes_get_value_extra($node, 'webform_attributes_wrapper_data_field'),
    '#title'         => t('Wrapper HTML attributes'),
    '#description'   => t('Key-value pairs MUST be specified as "attribute_name|attribute_value". Use of only alphanumeric characters and underscores is recommended in keys. One attribute per line.'),
    '#required'      => FALSE,
    '#weight'        => -9,
    '#element_validate' => array('_webform_attributes_validate_options_attributes'),
    '#parents' => array('extra', 'webform_attributes_wrapper_data_field'),
  );

  if (isset($form['display'])) {
    $form['display']['extra']['webform_attributes_data_field']         = $attribute_for_input;
    $form['display']['extra']['webform_attributes_wrapper_data_field'] = $attribute_for_wrapper;
  }
  else {
    $form['extra']['webform_attributes_data_field']         = $attribute_for_input;
    $form['extra']['webform_attributes_wrapper_data_field'] = $attribute_for_wrapper;
  }
}

/**
 * Element validation callback. Ensure keys are not duplicated.
 *
 * This function is based on _webform_edit_validate_select.
 */
function _webform_attributes_validate_options_attributes($element) {
  if (empty($element['#value'])) {
    return TRUE;
  }

  $lines = explode(PHP_EOL, trim($element['#value']));
  $duplicate_keys = $existing_keys = array();
  $group = '';
  foreach ($lines as $line) {
    $matches = array();
    $line = trim($line);
    if (preg_match('/^\<([^>]*)\>$/', $line, $matches)) {
      $group = $matches[1];
      // No need to store group names.
      $key = NULL;
    }
    elseif (preg_match('/^([^|]*)\|(.*)$/', $line, $matches)) {
      $key = $matches[1];
    }

    if (isset($key)) {
      if (isset($existing_keys[$group][$key])) {
        $duplicate_keys[$key] = $key;
      }
      else {
        $existing_keys[$group][$key] = $key;
      }
    }
  }

  if (!empty($duplicate_keys)) {
    $message = 'The Keys of the <strong>@title</strong> must be unique. The following keys have been used multiple times:!duplicates';
    $replaces = array(
      '@title' => $element['#title'],
      '!duplicates' => theme('item_list', array('items' => $duplicate_keys)),
    );
    form_error($element, t(filter_xss($message), $replaces));
  }
}

/**
 * Get default value form component.
 *
 * @param object $node
 *   Full node loaded.
 * @param string $find_extra_element
 *   String to find in webform component.
 *
 * @return string|null
 *   Return the value if found it.
 */
function _webform_attributes_get_value_extra($node, $find_extra_element) {
  $default_value = NULL;
  $cid = arg(4);

  if ($cid === 'new') {
    return $default_value;
  }
  $extra_elements = $node->webform['components'][$cid]['extra'];
  return !empty($extra_elements[$find_extra_element]) ? $extra_elements[$find_extra_element] : $default_value;
}

/**
 * Put attributes inside component attribute property.
 *
 * @param array $element
 *   The Drupal form element.
 * @param array $attributes
 *   An array with attributes to apply on component.
 * @param string $element_key
 *   The key of component Ex: '#attributes' or '#wrapper_attributes'.
 */
function _webform_attributes_inside_attributes(array &$element, array $attributes, $element_key = '#attributes') {
  foreach ($attributes as $attribute => $value) {
    $attribute = check_plain($attribute);
    $element[$element_key][$attribute] = $value;
  }
}

/**
 * Alias for list_extract_allowed_values.
 *
 * @param string $string
 *   The list of allowed values in string format.
 *
 * @return bool|array
 *   The array of extracted key/value pairs, or FALSE if the string is invalid.
 *
 * @see list_extract_allowed_values()
 */
function _webform_attributes_parser_string_to_array($string) {
  if (!empty($string)) {
    return list_extract_allowed_values($string, 'list_text', FALSE);
  }
  return FALSE;
}

/**
 * Alias for list_allowed_values_string.
 *
 * @param array $attributes
 *   An array of values, where array keys are values and array values are
 *   labels.
 *
 * @return bool|string
 *   The string representation of the $values array:
 *    - Values are separated by a carriage return.
 *    - Each value is in the format "value|label" or "value".
 *
 * @see list_allowed_values_string()
 */
function _webform_attributes_parser_array_to_string(array $attributes) {
  if (!empty($attributes)) {
    return list_allowed_values_string($attributes);
  }
  return FALSE;
}

/**
 * Get which components allowed add html attributes.
 *
 * @see webform_components()
 *
 * @return array
 *   Return an array with which components allowed add html attributes.
 */
function webform_attributes_get_which_component_allowed_html_attribute() {

  $components    = webform_components(TRUE);
  $default_value = drupal_map_assoc(array_keys($components));
  $results       = variable_get('webform_attributes_which_component_allowed_html_attribute', $default_value);
  return array_filter($results);
}
